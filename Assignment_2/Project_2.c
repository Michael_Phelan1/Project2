
#include <stdio.h>



struct player {char name[20];
               int disk;
               int NumofDisks;
			   };

//this function will return -1 if move is skipped
int player_move(char board[9][9], struct player person);
int move_checker(char board[9][9], int row, int column, char colour, char oppositeColour);
void printBoard(char board[9][9]);

int number_of_plays = 0;


int main(void)
{
    // Fill in the player's details
    struct player player_1, player_2;

    puts("Input the name of player one");
    scanf("%s", &player_1.name);
    puts("Input the name of player two");
    scanf("%s", &player_2.name);
    puts("Player one may choose which colour they will be, input 1 for dark or 0 for white");
    scanf("%d", &player_1.disk);
    player_1.NumofDisks = 0;
    player_2.NumofDisks = 0;

    if (player_1.disk == 1)
    {
        puts("Player one is dark, player two is white");
        player_2.disk = 0;
        printf("%s will go first\n", player_1.name);
    }
    if (player_1.disk == 0)
    {
        puts("Player one is white, player two is dark");
        player_2.disk = 1;
        printf("%s will go first\n", player_2.name);
    }

    // Initialize the board

    char board[9][9];
    char *boardPtr;
    boardPtr = &board[0][0];

    // Fill the board with values
    for (size_t i = 0; i<9; i++)
    {
            for(size_t j=0; j<9; j++)
                board[i][j] = 'x';
    }
    board[4][4] = '1';
    board[4][5] = '0';
    board[5][4] = '0';
    board[5][5] = '1';
    board[0][0] = ' ';
    char num = '1';
    for (size_t i = 1; i<9; i++)
    {
        board[0][i] = num;
        board[i][0] = num;
        num++;
    }


    // Printing the board

     for (size_t i = 0; i<9; i++)
    {
        for (size_t j = 0; j<9; j++)
        {
            printf("%c |", board[i][j]);
        }
        printf("\n");
    }

    // the loop that plays the game

    while (number_of_plays < 60){

            if (player_1.disk == 1 && number_of_plays % 2 == 0){
                    printf("\n%s's move\n", player_1.name);
                if (player_move(board, player_1) == -1)
                {
                    number_of_plays--;
                    break;
                }

                printBoard(board);
            }

            if (player_2.disk == 1 && number_of_plays % 2 == 0){
                    printf("\n%s's move\n", player_2.name);
              if (player_move(board, player_2) == -1)
                {
                    number_of_plays--;
                    break;
              }

                printBoard(board);
            }

            if (player_1.disk == 0 && number_of_plays % 2 != 0){
                    printf("\n%s's move\n", player_1.name);

                   if (player_move(board, player_1) == -1)
                     {
                        number_of_plays--;
                        break;
                     }

                printBoard(board);
            }

            if (player_2.disk == 0 && number_of_plays % 2 != 0){
                    printf("\n%s's move\n", player_2.name);
                if (player_move(board, player_2) == -1)
                    {
                        number_of_plays--;
                        break;
                    }

                printBoard(board);
            }



            number_of_plays++;
    }

// Checking to see who has won the game
    if (player_1.disk == 0)
    {
        for (size_t i = 1; i<9; i++)
        {
            for (size_t j =1; j<9; j++)
            {
                if (board[i][j] == '0')
                {
                    player_1.NumofDisks++;
                }
                else if (board[i][j] == '1')
                {
                    player_2.NumofDisks++;
                }
            }
        }
    }
if (player_1.disk == 1)
    {
        for (size_t i = 1; i<9; i++)
        {
            for (size_t j = 1; j<9; j++)
            {
                if (board[i][j] == '1')
                {
                    player_1.NumofDisks++;
                }
                else if (board[i][j] == '0')
                {
                    player_2.NumofDisks++;
                }
            }
        }
    }
// Printing the name of the winner
if (player_1.NumofDisks > player_2.NumofDisks)
{
    printf("%s is the winner\n", player_1.name);
    printf("%s is the loser\n", player_2.name);
}

else if (player_1.NumofDisks < player_2.NumofDisks)
{
    printf("%s is the winner\n", player_2.name);
    printf("%s is the loser\n", player_1.name);
}
if (player_1.NumofDisks == player_2.NumofDisks)
{
    puts("The game was tied");
}
    return 0;
}



// Function to illustrate the players move and check if it is valid
int player_move(char board[9][9], struct player person)
{
	int row,column, valid_move=0;

	printf("Enter where you would like to place your counter\n");
	printf("Enter 0 for row and column if there is no more moves to be made\n");

	printf("Row : ");
	scanf("%d", &row);


	printf("Column : ");
	scanf("%d", &column);


    if (row == 0 && column == 0)
    {
        printf("You have indicated that no more moves can be made\n");
        return -1;
    }
    else{

    if (person.disk == 1)
       {
           char colour = '1';
           char oppositeColour = '0';
           // Calling the function to check if the move is valid
           valid_move = move_checker(board, row, column, colour, oppositeColour);
           if (valid_move == 1)
            board [row][column] = '1';
            else{
                printf("\nYour move is not valid, try again\n\n");
                player_move(board, person);
            }
       }
    if (person.disk == 0)
        {
            char colour = '0';
            char oppositeColour = '1';
            // Calling the function to check if the move is valid
            valid_move = move_checker(board, row, column, colour, oppositeColour);
            if (valid_move == 1)
            board [row][column] = '0';
            else{
                printf("\nYour move is not valid, try again\n\n");
                player_move(board, person);
            }
        }


    return 1;
    }

}



// Function to check if the user's move is a valid one
int move_checker(char board[9][9], int row, int column, char colour, char oppositeColour)
{
// Scans through the board for squares with disks in them
    if ((board[row][column] == 'x' && board[row][column+1] == oppositeColour && board[row][column+2] == colour)){
        board[row][column+1] = colour;
        return 1;
    }
    if ((board[row][column] == 'x' && board[row][column-1] == oppositeColour && board[row][column-2] == colour)){
        board[row][column-1] = colour;
        return 1;
    }
    if ((board[row][column] == 'x' && board[row+1][column] == oppositeColour && board[row+2][column] == colour)){
        board[row+1][column] = colour;
        return 1;
    }
    if ((board[row][column] == 'x' && board[row-1][column] == oppositeColour && board[row-2][column] == colour)){
        board[row-1][column] = colour;
        return 1;
    }
    else
        return 0;
}



// Function to print out the board after each move
void printBoard(char board[9][9])
{
    for(int i=0; i<9; i++){
            printf("\n");
        for (int j=0; j<9; j++)
            printf("%c |", board[i][j]);
    }

    printf("\n");

}
